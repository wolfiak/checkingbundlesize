import { TestBed, inject } from '@angular/core/testing';

import { ShowElemntService } from './show-elemnt.service';

describe('ShowElemntService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ShowElemntService]
    });
  });

  it('should be created', inject([ShowElemntService], (service: ShowElemntService) => {
    expect(service).toBeTruthy();
  }));
});
