import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ShowElemntService} from "./show-elemnt.service";
import {Subject} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements AfterViewInit {
  chart1 = 'chart1';
  chartSubject: Subject<boolean>;
  chart2 = 'chart2';
  chartSubject2: Subject<boolean>;
  chart3 = 'chart3';
  chartSubject3: Subject<boolean>;
  colorScheme = {
    domain: ['#1976d2', '#d22e19']
  };
  single: any[] = [
    {
      name: 'ng2charts',
      value: 781.58
    },
    {
      name: 'ngx-charts',
      value: 1050
    }
  ];
  stars: any[] = [
    {
      name: 'ng2charts',
      value: 1200
    },
    {
      name: 'ngx-charts',
      value: 1986
    }
  ];
  starsCharts: any[] = [
    {
      data: [1200],
      label: 'ng2charts'
    },
    {
      data: [1986],
      label: 'ngx-charts'
    }
  ];
  singleCharts: number[] = [
    781.58, 1050
  ];
  labels: string[] = [
    'ng2charts', 'ngx-charts'
  ];
  options = {
    responsive: true,
  };
  public colors: Array<any> = [
    {
      backgroundColor: 'rgba(25,118,210,0.7)',
      borderColor: 'rgba(25,118,210)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];

  constructor(private sh: ShowElemntService) {

  }

  ngAfterViewInit() {
    this.chartSubject = this.sh.getObservable(this.chart1);
    this.chartSubject2 = this.sh.getObservable(this.chart2);
    this.chartSubject3 = this.sh.getObservable(this.chart3);
  }

  onSelect(event) {
    console.log(event);
  }
}
