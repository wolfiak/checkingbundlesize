import {Directive, ElementRef, HostListener, Input, OnDestroy, OnInit} from '@angular/core';
import {ShowElemntService} from './show-elemnt.service';
import {Subject} from 'rxjs';

@Directive({
  selector: '[appHoverElemnet]'
})
export class HoverElemnetDirective implements OnInit, OnDestroy {
  @Input() appHoverElemnet: string;
  private listener: Subject<boolean>;
  constructor(el: ElementRef, private sh: ShowElemntService) {
  }

  ngOnInit() {
   this.listener = this.sh.addListener(this.appHoverElemnet);
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.showElement();
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.hideElement();
  }

  private showElement(): void {
    this.listener.next(true);
  }

  private hideElement(): void {
    this.listener.next(false);
  }

  ngOnDestroy() {
    this.listener.complete();
  }
}
