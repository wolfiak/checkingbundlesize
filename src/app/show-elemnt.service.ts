import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';


export interface Subjector {
  key: string;
  subject: Subject<boolean>;
}


@Injectable({
  providedIn: 'root'
})
export class ShowElemntService {
  private list = new Array<Subjector>();

  constructor() {
  }

  addListener(str): Subject<boolean> {
    if (this.list.filter((elm) => elm.key.localeCompare(str))) {
      const sub = new Subject<boolean>();
      this.list.push({
        key: str,
        subject: sub
      });
      console.log(this.list);
      return sub;
    }
  }


  getObservable(str): Subject<boolean> {
    const tmp = this.list.find((elm) => {
      return elm.key.localeCompare(str) === 0;
    });
    return tmp.subject;
  }
}
